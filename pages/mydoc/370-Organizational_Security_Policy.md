---
title: Organizational Security Policy
keywords: security assessment, security policy, threat assessment, organizational security policies, policy templates
last_updated: February 18, 2020
tags: [organization_security, articles]
summary: "An organization is requesting assistance to create a security policy. The organization may or may not have already undergone a security assessment."
sidebar: mydoc_sidebar
permalink: 370-Organizational_Security_Policy.html
folder: mydoc
conf: Public
lang: en
---


# Organizational Security Policy
## Basic guide to help organizations create and implement a security policy

### Problem

Organizations, especially small ones, often lack written or formal security policies. Because these organizations already exist and do work, they will most likely already have informal practices and policies across the organization. Our aim is to conduct a security assessment in order to better understand their risks and existing work and informal practices, and help them create their own security policies to capture these practices.


* * *


### Solution

First schedule a call with the client and perform a light-weight security assessment with the organization following [Article #200](200-Lightweight_Security_Assessment.html).

Once you have conducted the assessment and noted the biggest concerns with the organization, **together with the point person of the organization** use the following templates to create a draft policy adapted to their priorities:

- **Templates for organizations**:
    - **English**:
        - [GDocs](https://docs.google.com/document/d/1JpNS9pDHSNOtJv7bM9qn--95Z4U4feq9tyHM54z7Ybs/edit)
        - [Markdown on Gitlab.com](https://gitlab.com/AccessNowHelpline/helpline_documentation_resources/-/blob/master/templates/Organizational_Security_Policies-Templates/Organizational_Security_Policies-Template.md)
        - [.odt on Gitlab.com](https://gitlab.com/AccessNowHelpline/helpline_documentation_resources/-/blob/master/templates/Organizational_Security_Policies-Templates/Organizational_Security_Policies-Template.odt)
    - **Spanish**
        - [GDocs](https://docs.google.com/document/d/1a3tg4XUvy5GEH50gpAd3ox7BmBszbsyN905gAOP0cG4/edit)
        - [Markdown on Gitlab.com](https://gitlab.com/AccessNowHelpline/helpline_documentation_resources/-/blob/master/templates/Organizational_Security_Policies-Templates/Plantilla_de_Politicas_de_Seguridad_Organizacional.md)
        - [.odt on Gitlab.com](https://gitlab.com/AccessNowHelpline/helpline_documentation_resources/-/blob/master/templates/Organizational_Security_Policies-Templates/Plantilla_de_Politicas_de_Seguridad_Organizacional.odt)

If the group your working with is not a formal organization, but a grassroots group of volunteers, you might find the **template for activists** more helpful as a starting point:
    - [Markdown on Gitlab.com](https://gitlab.com/AccessNowHelpline/helpline_documentation_resources/-/blob/master/templates/Organizational_Security_Policies-Templates/Activists_Security_Policy_Template.md)
    - [.odt on Gitlab.com](https://gitlab.com/AccessNowHelpline/helpline_documentation_resources/-/blob/master/templates/Organizational_Security_Policies-Templates/Activists_Security_Policy_Template.odt)

It is important to note that such a big document can be intimidating to the organization, so incident handlers should follow up closely with the client to clear any doubts and concerns. Consider whether sending the full document at once or if it is better to work on one section at a time and only add more when the previous sections are clear.

When the policy document has reached a satisfactory state, schedule a call with the client to review the full document one last time and make sure they understand and have the means to implement the policies.
* * *


### Comments

Follow the development of [SOAP](https://usesoap.app/), an organizational security policy generator currently in beta.


* * *


### Related Articles

- [Article #200: Lightweight Security Assessment](200-Lightweight_Security_Assessment.html)
