---
title: Ubuntu - Linux - FDE after OS installation
keywords: file encryption, Mac, GpgTools, backup
last_updated: July 19, 2018
tags: [devices_data_security, articles]
summary: "The client would like to encrypt the entire hard drive of a laptop running on Linux Ubuntu."
sidebar: mydoc_sidebar
permalink: 77-Ubuntu_Linux_FDE_After_Installation.html
folder: mydoc
conf: Public
lang: en
---


# Ubuntu - Linux - FDE after OS installation
## Is it possible to perform a FDE on a linux system after the initial installation.

### Problem

Full-disk encryption on Linux is hard to achieve if not performed during the initial installation phase.


* * *


### Solution

Full-disk encryption on Ubuntu is difficult to implement after the installation process has been completed. Currently there is not a user-friendly solution to achieve this. Some technical considerations make this process a non-trivial task.

However, the user can easily encrypt their home partition, where usually most of their sensitive files will be located. A guide on how to encrypt the users home and system swap can be found [here](http://www.howtogeek.com/116032/how-to-encrypt-your-home-folder-after-installing-ubuntu/).

If for some special reason the user needs to encrypt the entire hard drive, we can suggest them to back up their data and reinstall the system, making sure this time the full-disk encryption option is selected.


* * *


### Comments



* * *

### Related Articles
