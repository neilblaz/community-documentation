---
title: Arabic - Feedback Survey Template
keywords: email templates, client feedback, case handling policy, followup
last_updated: March 14, 2019
tags: [helpline_procedures_templates, templates]
summary: "الإيميل الذي يرسل للعملاء و للمتصلين عند إغلاق الحادثة "
sidebar: mydoc_sidebar
permalink: 344-ar_Feedback_Survey.html
folder: mydoc
conf: Public
ref: feedback-survey
lang: ar
---


# Arabic - Feedback Survey Template
## الإيميل الذي يرسل للعملاء و للمتصلين عند إغلاق الحادثة 


### Body

مرحبا $ClientName،

شكرا لتواصلك مع فريق مساعدو الأمن الرقمي، الذي تديره المنظمة العالمية لحقوق الإنسان اكساس ناو https://accessnow.org.

هذه الرسالة لأعلمك بغلق حادثك الذي بعنوان "{$Ticket-&gt;Subject}".

تقيمك مهم بالنسبة لنا. إذا كنت تود أن توفر لنا تقيمك حول تعاملك مع فريق مساعدو الأمن الرقمي، نرجو منك ملء استمارة التقييم التالية :

https://form.accessnow.org/index.php/958432/newtest/Y?958432X2X19={$Ticket-&gt;id}

رقم حدثك : {$Ticket-&gt;id}

إذا كان لديك أي أسئلة أو استفسارات أخرى أرجو أن تعلمنا و سوف نكون سعداء بتقديم المساعدة.

شكرا،
$IHName
