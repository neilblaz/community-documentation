---
title: PGP - User ID added
keywords: email, PGP, user ID, key management, Enigmail, Thunderbird, email security
last_updated: November 5, 2018
tags: [secure_communications_templates, templates]
summary: "Email to client to confirm that their new user ID has been added to their PGP key"
sidebar: mydoc_sidebar
permalink: 20-PGP-UserID_added.html
folder: mydoc
conf: Public
ref: PGP-UserID_added
lang: en
---


# PGP - User ID added
## Email to client to confirm that their new user ID has been added to their PGP key

### Body

Dear [Name],

I can see in the key servers that your new user ID [new email address] is now linked to your PGP key.

I will close this case, but I'm glad to help further in case you need more assistance. 

Regards,

[IH's Name]



* * *


### Related Articles

- [Article #19: PGP - Add UserID](19-PGP_add_userID.html)
