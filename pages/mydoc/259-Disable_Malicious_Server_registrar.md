---
title: Disable C&amp;C Server - Email to Registrar of Malicious Domain
keywords: C&amp;C server, malware, email templates, registrar, malicious website, malicious domain
last_updated: July 20, 2018
tags: [vulnerabilities_malware_templates, templates]
summary: "Template for writing to the registrar of a malicious domain to disable it"
sidebar: mydoc_sidebar
permalink: 259-Disable_Malicious_Server_registrar.html
folder: mydoc
conf: Public
ref: Disable_Malicious_Server_registrar
lang: en
---


# Disable C&amp;C server - email to registrar of malicious domain
## Template for writing to the registrar of a malicious domain to disable it

### Body

Dear XXXX,

I hope this email finds you well. I am [IH's name] from Access Now's Digital Security Helpline team - https://www.accessnow.org/help. I'm respectfully contacting you to report an incident that has been reported to us.

The IP address X.X.X.X (currently hosting domain "X.X.X") is being used as a Command and Control server which is identified to be used by a targeted hacking attack that is infecting civil society members and compromising their devices.

We would like to bring this issue to your attention and kindly ask for you assistance to disable the server.

Thank you very much in advance for your assistance. I look forward to hearing back from you.

Kind regards,

[IH's name]


* * *


### Related Articles

- [Article #219: Targeted Malware: Disable Malicious C&amp;C Server](219-Targeted_Malware_Disable_Malicious_Server.html.html)
- [Article #260: Disable C&amp;C server - email to hosting provider](260-Disable_Malicious_Server_hosting_provider.html)
