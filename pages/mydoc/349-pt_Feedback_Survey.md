---
title: Portuguese - Feedback Survey Template
keywords: email templates, client feedback, case handling policy, followup
last_updated: March 14, 2019
tags: [helpline_procedures_templates, templates]
summary: "Email to be sent to Portuguese-speaking clients or requestors when closing a ticket"
sidebar: mydoc_sidebar
permalink: 349-pt_Feedback_Survey.html
folder: mydoc
conf: Public
ref: feedback-survey
lang: pt
---


# Feedback Survey Template [Portuguese]
## Email to be sent to Portuguese-speaking clients or requestors when closing a ticket

### Body

Olá $ClientName,

Obrigado por entrar em contato com a Digital Security Helpline, administrada pela organização internacional de direitos humanos Access Now - https://accessnow.org.

Esta mensagem é para notificar sobre o fechamento de seu caso intitulado "{$ ticket&gt; Assunto}".

A sua opinião é importante para nós. Se você deseja deixar comentarios sobre sua experiência com a Linha de Ajuda em Segurança Digital do Access Now, preencha a seguinte pesquisa:

https://form.accessnow.org/index.php/958432/newtest/Y?958432X2X19={$Ticket-&gt;id}

Seu número de caso é: {$ Ticket-&gt; id}

Se você tiver outras dúvidas ou preocupações, informe-nos e teremos prazer em ajudar.

Obrigado,

$IHName
