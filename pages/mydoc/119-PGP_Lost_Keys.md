---
title: PGP - Lost Keys
keywords: email, PGP, key management, lost keys, email security
last_updated: November 6, 2018
tags: [secure_communications, articles]
summary: "The client has lost their PGP keys after formatting their machine and has no backup of their key pair."
sidebar: mydoc_sidebar
permalink: 119-PGP_Lost_Keys.html
folder: mydoc
conf: Public
lang: en
---


# PGP - Lost Keys
## How to help a client who has lost their PGP keys and hadn't backed them up

### Problem

A client has lost their key pair because their computer is lost or they have reinstalled the system and their key pair had not been backed up beforehand.

The client has not generated a revocation certificate or has lost it.

The client has no way of revoking the key, letting others know they will not be using it again.


* * *


### Solution

1. If the client has still access to the device where their key pair was stored, try to help them recover it using one the following tools:

    - [Recuva for Windows](https://www.ccleaner.com/recuva)
    - [Disk Drill for Mac](https://www.cleverfiles.com/recover-deleted-files-mac.html)
    - [Foremost for Linux](http://foremost.sourceforge.net/)
    - [Magic Rescue for Linux](http://www.linux-magazine.com/Issues/2015/180/Command-Line-Magic-Rescue)

2. If recovering the key does not succeed, recommend the following steps to the client:

    - Create a new key pair (see [Article #18: FAQ - PGP Setup](18-FAQ-PGP_Setup_Base_Article.html) for details).
    - Back up the new key pair in a secure device (e.g. an encrypted USB stick or SD card).
    - Update email signature and business cards with the new PGP key ID and fingerprint.
    - Ask contacts to delete the old key from their keyring.
    - Ask contacts and friends to sign the new public key to increase the level of trust for the new key.
  

* * *


### Comments



* * *


### Related Articles

- [Article #18: FAQ - PGP Setup](18-FAQ-PGP_Setup_Base_Article.html)
