---
title: Instructions to set Up FileVault 2 on a Mac - Russian
keywords: Device security, FileVault
last_updated: February 6, 2019
tags: [devices_data_security_templates, templates]
summary: "Шаблон письма для помощи клиентам в настройке FileVault 2 для Mac"
sidebar: mydoc_sidebar
permalink: 316-FileVault2_Mac_ru.html
folder: mydoc
conf: Public
ref: FileVault2_Mac
lang: ru
---


# Instructions to set Up FileVault 2 on a Mac
## Email template to guide clients into setting up FileVault 2 in their Mac 

### Body

Здравствуйте,

В этом письме рассказано, как настроить FileVault 2 на вашем компьютере. Пожалуйста, следуйте пошаговой инструкции и дайте мне знать, если появятся вопросы.

1. Чтобы избежать случайной потери данных, создайте резервную копию. 

    Для хранения резервной копии подготовьте внешний жесткий диск. Чтобы создать резервные копии файлов, можно пользоваться Time Machine или OS X Server вашей сети.

- Инструкция по созданию резервной копии файлов на внешнем жестком диске с помощью Time Machine: https://support.apple.com/ru-ru/HT201250

- Инструкция по начальной настройке устройства Time Capsule в локальной сети: https://support.apple.com/ru-ru/HT201510

    Пожалуйста, обратите внимание, что на создание первой локальной копии может потребоваться время.

2. Включите FileVault, следуя этим инструкциям: http://support.apple.com/ru-ru/HT4790

    При настройке FileVault выберите один из двух вариантов:

    - Разрешить iCloud снимать защиту с моего диска
    - Создать ключ восстановления и не использовать мою учетную запись iCloud

    Второй вариант безопаснее, если вы можете хранить ключ восстановления на USB-флешке, а флешку в надежном месте (и никому не давать к ней доступ).

Пожалуйста, имейте в виду: полное дисковое шифрование эффективно защищает информацию, только если компьютер выключен.

Подробнее о полном дисковом шифровании можно прочесть здесь: https://ssd.eff.org/ru/module/%D0%BE%D0%B1%D0%B5%D1%81%D0%BF%D0%B5%D1%87%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B1%D0%B5%D0%B7%D0%BE%D0%BF%D0%B0%D1%81%D0%BD%D0%BE%D1%81%D1%82%D0%B8-%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%85
 
Если нужна дальнейшая помощь, пожалуйста, дайте знать. Мы будем рады помочь.

С уважением,

[ИМЯ СОТРУДНИКА]


* * *


### Related Articles
