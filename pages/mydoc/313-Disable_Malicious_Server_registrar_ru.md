---
title: Disable C&amp;C Server - Email to Registrar of Malicious Domain - Russian
keywords: C&amp;C server, malware, email templates, registrar, malicious website, malicious domain
last_updated: February 6, 2019
tags: [vulnerabilities_malware_templates, templates]
summary: "Шаблон письма регистратору домена злоумышленника с просьбой отключить домен"
sidebar: mydoc_sidebar
permalink: 313-Disable_Malicious_Server_registrar_ru.html
folder: mydoc
conf: Public
ref: Disable_Malicious_Server_registrar
lang: ru
---


# Disable C&amp;C server - email to registrar of malicious domain
## Template for writing to the registrar of a malicious domain to disable it

### Body

Здравствуйте,

Меня зовут [ИМЯ СОТРУДНИКА]. Я работаю в Службе поддержки по вопросам цифровой безопасности организации Access Now (https://www.accessnow.org/help). Пожалуйста, обратите внимание на проблему, о которой нам сообщили пользователи.

IP-адрес [IP-АДРЕC] (который в настоящее время соответствует домену [ДОМЕННОЕ ИМЯ]) используется в качестве сервера управления (C&amp;C) для таргетированной хакерской атаки, от которой страдают гражданские активисты и их устройства.

Мы надеемся, что вы не останетесь равнодушны. Пожалуйста, помогите прекратить работу этого сервера.

Спасибо за вашу помощь. Буду ждать вашего ответа.

С уважением,

[ИМЯ СОТРУДНИКА]


* * *


### Related Articles

- [Article #219: Targeted Malware: Disable Malicious C&amp;C Server](219-Targeted_Malware_Disable_Malicious_Server.html.html)
- [Article #260: Disable C&amp;C server - email to hosting provider](260-Disable_Malicious_Server_hosting_provider.html)
