---
title: Content Restoration - Out of Mandate
keywords: censorship, content restoration, content moderation, out of mandate
last_updated: February 18, 2020
tags: [censorship_templates, templates]
summary: "Template to communicate to the client that their request for content restoration is out of mandate"
sidebar: mydoc_sidebar
permalink: 375-Content_Restoration_Out_of_Mandate.html
folder: mydoc
conf: Public
ref: Content_Restoration_Out_of_Mandate
lang: en
---


# Content Restoration - Out of Mandate
## Template to communicate to the client that their request for content restoration is out of mandate

### Body

Dear [$NAME],

My name is [$HANDLER_NAME], and I'm part of Access Now Digital Security Helpline. Thank you for reaching out.

While we understand the hardship you are going through, unfortunately, we cannot assist you with your request, as we have determined that it falls outside of the scope of assistance of the Digital Security Helpline. 

We recommend you appeal the decision to remove your content directly to the platform. In order to do so, please follow the instructions in the following links:

- [Facebook](https://www.facebook.com/help/contact/260749603972907) (removed accounts)
- [Twitter](https://help.twitter.com/en/managing-your-account/locked-and-limited-accounts) (Help with locked / limited account)
- [Instagram](https://help.instagram.com/366993040048856)
- [YouTube](https://support.google.com/youtube/answer/185111?hl=en)

Please note that at Access Now we are concerned about the practices and policies governing content online. Part of our policy work includes providing recommendations to social media platforms to improve their response to content moderation issues, including ensuring that their decisions conform to the principles of transparency and proportionality, and that they provide direct access to an effective remedy for all users. The protection of your human rights, both online and off, is the utmost priority for us. Thus, we strongly encourage platforms to comply with international human rights law when crafting and deploying their content moderation practices.

If you would like to provide feedback on the Digital Security Helpline please feel free to follow this survey: 

https://form.accessnow.org/index.php/958432/newtest/Y?958432X2X19={$Ticket-&gt;id}

Kind regards,

[IH's Name]


* * *


### Related Articles

- [Article #374: Content Moderation - Requests for Content Restoration](374-content_restoration.html)
