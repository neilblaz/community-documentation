---
title: Security Measures for macOS Computers
keywords: Mac, macOS, high-risk users, security tips, Filevault, software security, firewall, stealth mode, Time Machine, Apple Analytics, automatic update, Gatekeeper, antivirus, antimalware
last_updated: October 13, 2019
tags: [devices_data_security, articles]
summary: "A high-risk macOS user is asking for digital security recommendations on how to secure their device."
sidebar: mydoc_sidebar
permalink: 159-Security_Mac.html
folder: mydoc
conf: Public
lang: en
---


#  Security Measures for macOS Computers
## Advice on digital security measures for high-risk macOS users

### Problem

High-risk users might not be protected sufficiently by the default security settings of macOS computers. We need to check that basic security settings are set up correctly and that the client is also using the safest software available.


* * *


### Solution

#### Initial assessment

We need to ask the client about their job and security context.

1. What do they use the computer for? 
2. Do they travel with it?
3. Do they have any specific needs?

If the client's threat model needs to be assessed more thoroughly, also see the questions in [Article #200: Lightweight Security Assessment](200-Lightweight_Security_Assessment.html).


#### Recommendations

These are some digital security best practices for user at risk:

1. **Hard drive encryption**: check that FileVault is enabled, and enable it if it's not.

    In recent Mac versions, FileVault is enabled by default. It is worth checking in any case that it is enabled in the client's computer. For more information, see [Article #104: Full-Disk Encryption on Mac with FileVault 2](104-FDE_Mac_FileVault2.html).

    - Instructions for enabling FileVault can be found [here](https://support.apple.com/en-us/HT204837).
    - More information on FileVault and full disk encryption can be found in Citizen Lab's [Security Planner](https://securityplanner.org/#/tool/mac-encryption).

2. **Enable the application firewall**.

    - For an explanation of what a firewall does and why it's important to enable it, we can send [this link](https://securityplanner.org/#/tool/mac-firewall) of the Security Planner to the client.
    - Instructions on how to enable Mac firewall can be found [here](https://support.apple.com/en-au/HT201642). 
    - When enabling the firewall, it's a good practice to also enable **stealth mode** - this will prevent an attacker in the same network to discover the machine using probing request like ICMP requests (ping). 
   
3. **Enable Encrypted Time Machine**

    Follow this [link](https://support.apple.com/en-us/HT201250) to help the user create an automatic and encrypted backup for their most sensitive files. This will help the user restore their important information in case it is lost or corrupted (e.g. in case they are targeted by ransomware). 

4. **Disable Apple Analytics**

    Users at risk should not share unnecessary data outside of their safe environment. Therefore it is a good practice to disable Apple Analytics in order to stop their machine from sending their usage information to Apple. Follow the instructions in this [link]([https://support.apple.com/guide/mac-help/share-analytics-information-mac-apple-mh27990/mac) to opt out of sharing analytics. 

5. Make sure that the **system automatic update** is enabled.

    - Send the client [this link with instructions to update their software and enable automatic software updates](https://support.apple.com/en-us/HT201541).
    - More information on why automatic software updates are important can be found in [this guide](https://securityplanner.org/#/tool/keep-your-mac-updated).

6.  Make sure **Gatekeeper is enabled**

    Gatekeeper is Apple's application white-listing control that stops applications from unverified sources from running without authorization. Disallowing unverified software will reduce the risk of unauthorized or malicious applications running on the system. Gatekeeper is turned on by default, but some users may choose to disable it or, in an extreme scenario, an attacker could disable it to make their exploit possible. We need to verify that Gatekeeper is enabled following the instructions in this [link](https://support.apple.com/en-us/HT202491) (section "View the app security settings on your Mac").

7. **Install an antivirus**, and consider installing an antimalware.

    For more instructions on antivirus tools for Mac, see [Article #128: Antivirus for Mac](128-Antivirus_for_Mac.html)

8. Suggest the client to use **Chromium or Firefox** as default browser and to install **addons for privacy and security**.

    For more instructions on browsing security, refer to [Article #212: Safe Browsing Practices and Plugins](212-Safe_Browsing_Practices.html).

9. Change the default search engine to DuckDuckGo.

10. Consider sending recommendations on basic security hygiene and on other security tools, such as password managers and VPNs.


* * *


### Comments


* * *


### Related Articles

- [Article #200: Lightweight Security Assessment](200-Lightweight_Security_Assessment.html)
- [Article #104: Full-Disk Encryption on Mac with FileVault 2](104-FDE_Mac_FileVault2.html)
- [Article #128: Antivirus for Mac](128-Antivirus_for_Mac.html)
- [Article #212: Safe Browsing Practices and Plugins](212-Safe_Browsing_Practices.html)
