# Access Now Digital Security Helpline FAQ documentation

Repository for Access Now Digital Security Helpline's public documentation.


- [**Website**](https://accessnowhelpline.gitlab.io/community-documentation/)
- [Original theme](http://idratherbewriting.com/documentation-theme-jekyll/)


## How to Contribute

If you are planning to edit a page of Access Now Helpline's [Community Documentation website](https://accessnowhelpline.gitlab.io/community-documentation/), you can identify the file that needs to be edited in this repository by clicking the "Edit me" button in the relevant web page.

If you want to translate a document, please see [these instructions](pages/mydoc/translation_readme.md)

### Suggesting an Edit

If you would like to suggest or discuss an edit, you can submit an [issue in this project](https://gitlab.com/AccessNowHelpline/community-documentation/issues).

You can learn how to create an issue in [this howto](https://docs.gitlab.com/ee/user/project/issues/create_new_issue.html).


### Editing and testing

If you are planning to add new texts or to rewrite an article or template, it is recommended to test your changes in your local machine before you push them to the repository. If you are doing a one-off contribution, you can [edit online and test quickly by downloading a fork of this repository with HTTPS](#edit_online). If, on the other hand, you are planning to contribute to this repository more than once, we suggest you [work in your local repository](#local_edit). Working [in your local repository](#local_edit) is also recommended if you need to upload many files at once.

What follows is a description of both procedures.

<a name="edit_online"></a>
#### Edit online and test locally (one-off)

What follows is a step-by-step guide to edit or add texts through the web interface and test your changes in your machine before you submit a merge request.

*Please note that if you're doing a minor change, you can skip the testing stage.*

1. Fork the `community-documentation` repository:

    Click the `Fork` button to fork the repository and assign it to your Gitlab user.

    ![Fork the repository](images/repo-fork.png)

    Also see [this guide on how to fork a repository](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork).

2. Edit or add files in your forked repository through the [web interface](https://docs.gitlab.com/ee/user/project/repository/web_editor.html).

3. Once you're happy with your changes, test them in your local machine. (*If you just did a minor change, you can skip to step 4.*)

    1. Download your edited fork in a .zip file:
        - Go to the main page of your fork repository (`https://gitlab.com/YourUserName/community-documentation`). Click the `SSH` button under the title of the repository, and select `HTTPS` in the drop-down menu.

            ![SSH/HTTPS selection](images/repo-ssh-https.png)

        - Click the drop-down menu to the right of the HTTPS address, and select `Download zip`.

            ![Download zip](images/repo-download.png)

    2. Unzip the folder you've just downloaded.

    3. Install Jekyll

        If you’ve never tested a Jekyll-based site locally on your computer, you will probably need to install [Jekyll](https://jekyllrb.com/) and its dependencies.

        You can find installation instructions for your operating system in [this page](https://jekyllrb.com/docs/installation/#guides).

    4. Install Bundler with this command:

            gem install bundler

    5. You’ll want Bundler to make sure all the needed Ruby gems work well with your project. Bundler sorts out dependencies and installs missing gems or matches up gems with the right versions based on gem dependencies. To do so, launch these commands:

            cd /path/to/community-documentation
            
            bundle install

    6. Finally, test your changes by launching this command **within the `community-documentation` repository**:

            bundle exec jekyll serve

        If the command is successful, you will be able to visualize your test website by opening a browser and entering `http://127.0.0.1:4000/community-documentation/` in the address bar.

4. If the test works fine, you can [submit a merge request to add your changes to the main repository](https://docs.gitlab.com/ee/workflow/forking_workflow.html#merging-upstream).

<a name="local_edit"></a>
#### Edit and test in your local machine

**This is the recommended procedure if you need to upload many files (e.g. images) at once**.

What follows is a step-by-step guide to download a synchronized clone of your forked repository, edit or add texts, test your changes in your machine, and update the central repository with your changes.

*Please note that if you're doing a minor change, you can skip the testing stage.*

1. Generate an SSH key pair and add your SSH public key to your Gitlab.com account.

    - [Generate an SSH key pair](https://docs.gitlab.com/ee/ssh/#generating-a-new-ssh-key-pair)
    - [Add your public key to your account](https://docs.gitlab.com/ee/ssh/#adding-an-ssh-key-to-your-gitlab-account)


2. Fork the `community-documentation` repository:

    Click the `Fork` button to fork the repository and assign it to your Gitlab user.

    ![Fork the repository](images/repo-fork.png)

    Also see [this guide on how to fork a repository](https://docs.gitlab.com/ee/workflow/forking_workflow.html#creating-a-fork).

3. Clone your fork:

    1. Go to the main page of your fork repository (`https://gitlab.com/YourUserName/community-documentation`).

    2. Copy the address next to the `SSH` button. It will be something like: `git@gitlab.com:YourUserName/community-documentation.git`.

        ![SSH address](images/gitlab-ssh.png)

    3. Open a terminal, go to the location where you would like to download your work folder, and launch this command:

            git clone git@gitlab.com:YourUserName/community-documentation.git

        *Replace the above address with the SSH address you copied in the above step.*

        *You will be prompted to enter the password for your SSH key at this point.*

4. Edit or add files using your favourite markdown editor. All content can be found in the `pages/mydoc/` directory.

    If you need some basics on git, we suggest you to start from [this page](https://gist.github.com/joncamfield/9249d2442c5849335c9811b8b70e0bc6).

5. Once you're happy with your changes, test them in your local machine. (*If you just did a minor change, you can skip to step 6.*)

    1. Install Jekyll

        If you’ve never tested a Jekyll-based site locally on your computer, you will probably need to install [Jekyll](https://jekyllrb.com/) and its dependencies.

        You can find installation instructions for your operating system in [this page](https://jekyllrb.com/docs/installation/#guides).

    4. Install Bundler with this command:

            gem install bundler

    5. You’ll want Bundler to make sure all the needed Ruby gems work well with your project. Bundler sorts out dependencies and installs missing gems or matches up gems with the right versions based on gem dependencies. To do so, launch these commands:

            cd /path/to/community-documentation
            
            bundle install

    6. Finally, test your changes by launching this command **within the `community-documentation` repository**:

            bundle exec jekyll serve

        If the command is successful, you will be able to visualize your test website by opening a browser and entering `http://127.0.0.1:4000/community-documentation/` in the address bar.

6. If the test works fine, and you want to commit your changes to the main repository:

    - make sure that you have added your new files to the git repository with the command:

            git add [add file name/s here]

    - if you need to remove files, do it with this command:

            git rm [file name/s]
            
    - make sure you have committed your changes to your git repository with the command:

            git commit -a -m "add a comment here"

    - push your changes to your repository on Gitlab with the command: 

            git push origin master

        *You will be prompted to enter the password for your SSH key at this point.*

    - If you go to the main page of your forked repository, you will find a notification informing you that you have pushed changes. Congratulations: now your local and central repositories are synced!

7. At this point you only need to submit a merge request following [these instructions](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html).

    Thank you for contributing to Access Now Digital Security Helpline Community documentation!


### Creating New Articles or Templates

#### 0. Before Writing

A couple of steps are required before you even start writing:

- Make sure that you have a clear idea of the technical solutions required to solve the case type described in the article, and discuss them with your team and, if necessary, with your community. Also make sure that this documentation doesn't already exist.

- Make sure that the case can be generalized to a broader type of issues.


#### 1. Drafting an Article for RT

Draft the article in markdown in a new branch.

- When writing an article, use this [template](https://gitlab.com/AccessNowHelpline/community-documentation/blob/master/pages/mydoc/article_template.md).
- When writing a message template use this [template](https://gitlab.com/AccessNowHelpline/community-documentation/blob/master/pages/mydoc/email_template.md).

*Both these templates include a series of meta tags in the front
matter included between 3 hyphens at the top of the document. These meta tags
won't be visible in the final web page, but are important for the final website
deployment and for its layout, as well as for the continuous integration scripts.
They should be filled out as carefully as every other field in the templates. See ["YAML front matter"](#yaml) below for a description of each tag in the front matter.*


#### 2. Publication

1. Do a merge request so the administrators of this repository are notified about your proposed changes.

2. When the content has been reviewed, the administrators of this repository will merge your changes.


### Types of content

The content in this website is organized hierarchically, as outlined in the following list:

- **Overaching FAQs on a topic** - Overarching articles that describe how to deal with a broad incident typology, with general advice and warnings. These articles are often linked in the "Related Articles" section of other articles of the same class. The title of the article should start with "FAQ" - for example: "FAQ - Account Recovery and Deactivation". This type of content is defined by the `faq` tag in the "tags" value of the YAML front matter (see below "YAML front matter"). 

- **Articles on a topic** - These articles explain how to address a particular incident or problem with more detailed examples and instructions, and links to resources on specific platforms, tools or entities. These articles are often linked in the "Related Articles" section of other articles and templates.

- **Email templates** - templates connected to specific situations to respond to requests and/or reach out to users, partners, or peers.

If you plan to add a new FAQ or article to the repository, you can start by duplicating [this file](https://gitlab.com/AccessNowHelpline/community-documentation/blob/master/pages/mydoc/article_template.md).

If you are adding an email template, you can base it on [this file](https://gitlab.com/AccessNowHelpline/community-documentation/blob/master/pages/mydoc/email_template.md).

<a name="yaml"></a>

### YAML front matter

The YAML front matter (the keys and values included between `---` at the beginning of each article) contains metadata that is needed for rendering the website as well as to improve the search. Please follow these instructions to fill in the values correctly:

#### Articles YAML front matter

- title: Title of the article, the same you will use in the body of the text.

- keywords: topics (see "Articles categories and topics" below)

- last_updated: Month XX, 20XX

- tags: [category, articles, (faq)] - Replace "category" with one of the categories listed in "Articles categories and topics" below. Leave "articles" unchanged. Add "faq" if this is an overarching article on the topic (see "Types of content" above), or remove it if it is not.

- summary: Explain in one line, within quotes, what this article is about.

- sidebar: mydoc_sidebar - **leave this unchanged**.

- permalink: title_of_article.html - use the same name as the name of the file, but replace `.md` with `.html`

- folder: mydoc - **leave this unchanged**.

- conf: Public - **leave this unchanged**.

- ref: a unique identifier for each localized version of the same article. If you are creating a new article, this can be any identifier matching with the title, without spaces. If you are translating an existing article, the identifier needs to be identical to the one in the original version that you are translating.

- lang: add 2-letters code for the language used in the article.


#### Email templates YAML front matter

- title: Title of the template, the same you will use in the body of the text.

- keywords: topics (see "Email templates categories and topics" below)

- last_updated: Month XX, 20XX

- tags: [category, templates] -  Replace "category" with one of the categories listed in "Email templates categories and topics" below. Leave "templates" unchanged.
 
- summary: Explain in one line, within quotes, what this template is about.

- sidebar: mydoc_sidebar - **leave this unchanged**.

- permalink: title_of_article.html - use the same name as the name of the file, but replace `.md` with `.html`

- folder: mydoc - **leave this unchanged**.

- conf: Public - **leave this unchanged**.

- ref: a unique identifier for each localized version of the same article. If you are creating a new article, this can be any identifier matching with the title, without spaces. If you are translating an existing article, the identifier needs to be identical to the one in the original version that you are translating.

- lang: add 2-letters code for the language used in the article.


### Categories

#### Articles categories and topics

What follows is a list of all categories assigned to each of the FAQ articles and articles in this repository. Each category is followed by a series of topics, which can be assigned to each article in the "keywords" tag.

- **Access Now Procedures**: - Access Now's internal procedures
	- Secure Communications
	- Onboarding/Offboarding Handling Procedures
	- Name Policy

- **Account Recovery**: recovery, deletion and suspension of accounts on
  social networking platforms and email services
    - Facebook
        - WhatsApp
    - Google
    - Microsoft
    - Twitter
    - Wordpress
    - Yahoo

- **Account Security**: Recommendations and best practices for securing accounts on online services
    - Privacy
    - Authentication security

- **Anonymity and Circumvention**: Tools and tips for using the internet anonymously and for circumventing censorship
    - Anonymity
    - Circumvention
    
- **Browsing Security**: Recommendations and best practices for browsing the web securely

- **Censorship**: Solutions for censored websites

- **Data leaks**: Prevention, mitigation and hardening of infrastructure in case of data leaks

- **DDoS Attack**: Support for DDoS attacks mitigation and prevention

- **Defacement**: Mitigation and support in case of attacks on websites that change the visual appearance or the contents of the site or a webpage

- **Devices and data security and management**: Recommendations and best practices for secure storage, deletion, and backup
    - Backup security
    - Device Security
    - Secure storage
    - Mobile security
    - Software update
    - Device management

- **Direct interventions**: In-person interventions and financial support of specific groups
    - Digital security audits
    - Digital security trainings
    - Digital security clinics
    - Grants

- **Documentation** - Solutions and best practices for internal documentation and documentation workflows
    - Articles
    - Git
    - Resources

- **Fake Domain Mitigation**: Support to take down fake domains and accounts on web hosting platforms

- **Forensics**: Collection, preservation, and analysis of evidence in case of digital attacks on devices and systems

- **Harassment**: Prevention, support and mitigation in cases of harassment, intimidation, threats or attacks against a person or community

- **Helpline Procedures** - Digital Security Helpline internal procedures
    - Followup
    - Outreach
    - Vetting
    - Contact addresses
    - Hosting and Backup Policy
    - Mandate Check Policy
    - Case Handling Policy
    - Tor Abuse Handling Procedure

- **Infrastructure**: Recommendations, best practices, support and audits for infrastructure security
    - Infrastructure security
    - Infrastructure management
    - Website protection
    - Website management
    - Web penetration testing

- **Organization security**: Policies, assessments, audits and recommendations for organizations
    - Organizational security policies
    - Safe travels
    - Security Assessment

- **Phishing/Suspicious email**: Analysis of phishing/suspicious email and recommendations
  
- **Referrals** - Out-of-mandate situations where we know who to refer the client to
	- Malware analysis
	- Assessment
	- Physical security
	- Forensics
	- Advocacy
	- Legal

- **Secure Communications**: Procedures about how we can secure clients' communications
    - Email security
        - PGP
    - Instant messaging security
    - Voice/video calls security

- **Shutdown**: Reports, resilient solutions, outreach and mitigation in case of internet shutdowns
	- Shutdown resilient solutions

- **System compromise**: Mitigation and hardening of system in case of system compromise

- **Vulnerabilities and Malware**: vulnerabilities, malware prevention, malware triage and containment
	- Malware
	- Vulnerabilities

- **Web Vulnerabilities**: Reporting, outreach and support in case of vulnerabilities in web servers and hosting infrastructures


#### Email templates categories and topics

What follows is a list of all categories assigned to each of the email templates in this repository. Each category is followed by a series of topics, which can be assigned to each email template in the "keywords" tag.
  
- **Access Now Procedures Templates**
    - Secure Communications
    - Onboarding/Offboarding Handling Procedures
    - Name Policy
	
- **Account Recovery Templates**
    - Facebook
    - WhatsApp
    - Google
    - Microsoft
    - Twitter
    - Wordpress
    - Yahoo

- **Account security Templates**
    - Privacy
    - Authentication security

- **Anonymity and Circumvention Templates**
    - Anonymity
    - Circumvention
    
- **Browsing Security Templates**

- **Censorship Templates**

- **Data Leaks Templates**
    
- **DDoS Attack Templates**

- **Defacement Templates**

- **Devices and data security and management Templates**
    - Backup security
    - Device Security
    - Secure storage
    - Mobile security
    - Software update
    - Device management
    
- **Direct interventions Templates**
    - Digital security audits
    - Digital security trainings
    - Digital security clinics
    - Grants
      
- **Documentation Templates**
    - Articles
    - Git
    - Resources

- **Fake Domain Mitigation Templates**

- **Forensics Templates**
    
- **Harassment Templates**
    
- **Helpline Procedures Templates**
    - Followup
    - Outreach
    - Vetting
    - Hosting and Backup Policy
    - Mandate Check Policy
    - Case Handling Policy
    - Tor Abuse Handling Procedure
        
- **HL Infrastructure Management Templates**
    - Host Hardening
    - Virtualization
    - DNS Administration
    - Hosting Provider Contact
    - VPN Administration
    - SQL Administration
    - IDS/IPS Administration
    - Schleuder Administration
    - Web Server Administration
    - Mondo Administration
    - Monitoring System Administration
    - OpenSSL Administration
    - Tor Service Administration
    - IRC Administration
    - Web management Interface/KVM
    - MailMan
    - OoniProbe
    - RequestTracker
    - WordPress
        
- **Infrastructure Templates**
    - Infrastructure security
    - Infrastructure management
    - Website protection
    - Website management
    - Web penetration testing
    
- **Organization security Templates**
    - Organizational security policies
    - Safe travels
    - Security Assessment
        
- **Phishing/Suspicious email Templates**
    
- **Referrals Templates**
    - Malware analysis
    - Assessment
    - Physical security
    - Forensics
    - Advocacy
    - Legal

- **Secure Communications Templates**
    - Email security
        - PGP
    - Instant messaging security
    - Voice/video calls security
   
- **Shutdown Templates**
   	- Shutdown resilient solutions

- **System compromise Templates**

- **Vulnerabilities and Malware Templates**
    - Malware
    - Vulnerabilities
        
- **Web Vulnerabilities Templates**



